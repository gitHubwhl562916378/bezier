#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{

}

Widget::~Widget()
{

}

void Widget::resizeGL(int w, int h)
{
    pMatrix_.setToIdentity();
    pMatrix_.perspective(45,static_cast<float>(w)/h,0.01f,100.0f);
}

void Widget::initializeGL()
{
    render_.inititsize(QVector<BNPosition>() << qMakePair(0.0,0.0)
                       << qMakePair(0.4,-0.3) << qMakePair(0.4,0.3) << qMakePair(0.8,-0.6)
                       ,0.05,Qt::white);
    camera_.setX(0);
    camera_.setY(0);
    camera_.setZ(2);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0, 0.0, 0.0, 0.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D(0.0,0.0,0.0),QVector3D(0.0,1.0,0.0));

    QMatrix4x4 mMatrix;
    mMatrix.translate(0,0.4,0);
    int n = 360 / 5;
    for(int i = 0; i < n; i++){
        QMatrix4x4 yangleMatrix;
        yangleMatrix.rotate(i * 5,0,1,0);
        render_.render(f,pMatrix_,vMatrix,yangleMatrix * mMatrix);
    }
}
