#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "bezierrender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void resizeGL(int w,int h) override;
    void initializeGL() override;
    void paintGL() override;

private:
    BezierRender render_;
    QMatrix4x4 pMatrix_;
    QVector3D camera_;
};

#endif // WIDGET_H
